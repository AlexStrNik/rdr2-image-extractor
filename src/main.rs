use fltk::{
    app::*,
    button::Button,
    dialog::{FileChooser, FileChooserType, FileDialog},
    frame::Frame,
    input::*,
    prelude::*,
    window::*,
};
use std::{
    error::Error,
    fs,
    path::Path,
    sync::{Arc, Mutex},
};

fn main() {
    let app = App::default().with_scheme(AppScheme::Gtk);
    let mut window = Window::new(100, 100, 400, 280, "RDR2 Image Extractor");

    let input_folder = Arc::new(Mutex::new(None));
    let output_folder = Arc::new(Mutex::new(None));

    let mut frame = Frame::new(30, 20, 340, 20, "");
    let mut select_input = Button::new(30, 60, 340, 40, "Select RDR2 images folder");

    let input_folder1 = input_folder.clone();
    select_input.set_callback(move || {
        let mut dialog = FileChooser::new(
            &frame.label(),
            "",
            FileChooserType::Directory,
            "Select RDR2 images folder",
        );
        dialog.show();

        while dialog.shown() {
            fltk::app::wait();
        }

        if let Some(directory) = dialog.directory() {
            frame.set_label(&directory);
            *input_folder1.lock().unwrap() = Some(directory);
        }
    });

    let mut frame2 = Frame::new(30, 120, 340, 20, "");
    let mut select_input2 = Button::new(30, 160, 340, 40, "Select output folder");

    let output_folder1 = output_folder.clone();
    select_input2.set_callback(move || {
        let mut dialog = FileChooser::new(
            &frame2.label(),
            "",
            FileChooserType::Directory,
            "Select output folder",
        );
        dialog.show();

        while dialog.shown() {
            fltk::app::wait();
        }

        if let Some(directory) = dialog.directory() {
            frame2.set_label(&directory);
            *output_folder1.lock().unwrap() = Some(directory);
        }
    });

    let mut convert_button = Button::new(30, 220, 340, 40, "Convert");

    convert_button.set_callback(move || {
        let input_folder = input_folder.lock().unwrap();
        let output_folder = output_folder.lock().unwrap();

        if input_folder.is_some() && output_folder.is_some() {
            let files = fs::read_dir(input_folder.as_ref().unwrap()).unwrap();

            for file in files {
                let input_path = file.unwrap().path();

                let new_name = format!("{}.jpg", input_path.file_name().unwrap().to_str().unwrap());

                let output_path = Path::join(Path::new(output_folder.as_ref().unwrap()), new_name);

                procces_image(input_path.to_str().unwrap(), output_path.to_str().unwrap()).unwrap();
            }
        }
    });

    window.end();
    window.show();

    app.run().unwrap();
}

fn procces_image(input_path: &str, output_path: &str) -> Result<(), Box<dyn Error>> {
    let bytes = fs::read(input_path)?;
    let mut out_bytes = Vec::with_capacity(bytes.len());

    let mut previous = 0x00;
    let mut jpeg_data = false;

    for current in bytes.into_iter() {
        if jpeg_data {
            out_bytes.push(current);
        }

        if previous == 0xFF && current == 0xD8 && !jpeg_data {
            // SOI_Marker
            jpeg_data = true;
            out_bytes.push(previous);
            out_bytes.push(current);
        } else if previous == 0xFF && current == 0xD9 {
            // EOI_Marker
            break;
        }

        previous = current;
    }

    fs::write(output_path, out_bytes)?;

    Ok(())
}
