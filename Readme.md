# RDR2 Image Extractor

This tool can convert your Red Dead Redemption 2 Photos to normal JPG images.

RDR2 Photos are usually located in `\Documents\Rockstar Games\Red Dead Redemption 2\Profiles\[YourProfileName]`.

# Installation

If you have `cargo` installed you can simply type:

`cargo install --git https://gitlab.com/AlexStrNik/rdr2-image-extractor`

Otherwise, you need to download a binary from the Releases tab.